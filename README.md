# The Climate Emergency Hackathon

Valtech and Ofgem are hosting ‘The Climate Emergency Hackathon’ on Friday 31st January & Saturday 1st February 2020.

The UK needs to get a better handle on how it deals with carbon in order to decarbonise, recarbonise and effectively evaluate the impact of carbon on the environment. Energy is just one of the sectors that requires modernisation and innovation to enable a greener and sustainable future. Utilising data across all sectors, enabling data to be open, discoverable and easily linked is key to helping us reach our goal of net zero by 2050.

Following on from Valtech’s ‘Sustainability and the Climate Change Emergency’ event in October 2019, the aim of ‘The Climate Emergency Hackathon’ is to collate data across industries to start to answer big questions such as; How would we decarbonise the UK within 5 years? what is the best approach to cultural change? and how can we monitor our impact on biodiversity?

This two-day event is open to people of all disciplines, Creatives, UX/UI/UR, Developers, BAs, Data Engineers, Data Scientists etc. as well as people with industry knowledge who are interested in working within a team to utilise technology and data to contribute towards solving the climate emergency. All attendees will be split into self-contained project teams which will then work through a range of tasks to develop a data-driven solution. We also have industry experts from Valtech, Intel and Ofgem on site who are happy to help you get started if you bring the ideas!

Click the link below for more details on the event and to register for the event.

https://www.eventbrite.co.uk/e/the-climate-emergency-hackathon-registration-84728297619


This project holds schemas for all the data we will be releasimg for the hackthon, watch this space!

